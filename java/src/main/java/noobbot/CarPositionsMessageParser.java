package noobbot;

import java.util.List;
import java.util.Map;

import noobbot.agent.CarPositionInTrack;
import noobbot.commands.GameTickMsgWrapper;
import noobbot.track.Track;


public class CarPositionsMessageParser {
	
	@SuppressWarnings("rawtypes")
	private Map dataMap;
	
	@SuppressWarnings("rawtypes")
    public CarPositionsMessageParser(GameTickMsgWrapper msg) {
		super();
		this.dataMap = (Map)((List)msg.data).get(0);
	}
	
	public double getAngle() {
		return (double)dataMap.get("angle");
	}
	
	@SuppressWarnings("rawtypes")
    public double getPieceIndex() {
		return (double)((Map)dataMap.get("piecePosition")).get("pieceIndex");
	}
	
	@SuppressWarnings("rawtypes")
    public double getInPieceDistance() {
		return (double)((Map)dataMap.get("piecePosition")).get("inPieceDistance");
	}
	
	@SuppressWarnings("rawtypes")
    public double getLane() {
		return (double)((Map)((Map)dataMap.get("piecePosition")).get("lane")).get("endLaneIndex");
	}
	
	@SuppressWarnings("rawtypes")
    public double getLap() {
		return (double)((Map)dataMap.get("piecePosition")).get("lap");
	}
	
	public CarPositionInTrack createCurrentPosition(Track track) {
		return new CarPositionInTrack(track, getLane(), getPieceIndex(), getInPieceDistance(), getAngle(), getLap());
	}
}
