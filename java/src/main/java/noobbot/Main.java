package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import noobbot.agent.CarPositionInTrack;
import noobbot.agent.OponentAgent;
import noobbot.agent.RacingCarAgent;
import noobbot.commands.GameTickMsgWrapper;
import noobbot.commands.Join;
import noobbot.commands.Ping;
import noobbot.commands.SendMsg;
import noobbot.learning.release.ReleaseThrotleQValues;
import noobbot.learning.slowdown.SlowDownQValues;
import noobbot.physics.DistanceCalculator;
import noobbot.track.Track;

import com.google.gson.Gson;

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];
        
        int raceNB = 0;
        try {
	        Random rand = new Random();
	        String[] tracks = {"keimola", "germany", "france"};
		        ReleaseThrotleQValues.getInstance();
		        SlowDownQValues.getInstance();
		
		        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		
		        @SuppressWarnings("resource")
		        final Socket socket = new Socket(host, port);
		        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));
		
		        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		        
		        int randomNum = rand.nextInt(3);
		        
		        raceNB++;
		        if (randomNum > 2) randomNum = 2;
		        System.out.println("Race " + raceNB + " track " + tracks[randomNum]);
		        new Main(reader, writer, new Join(botName, botKey));
        } catch (Throwable t) {
        	t.printStackTrace();
        }
    }

    final Gson gson = new Gson();
    private final PrintWriter writer;

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;

        send(join);
        
        Track track = null;
        RacingCarAgent agent = null;
        DistanceCalculator distanceCalculator = new DistanceCalculator();
        Integer previousGameTick = null;
        boolean crashed = false;
        while ((line = reader.readLine()) != null) {
        	
            final GameTickMsgWrapper msgFromServer = gson.fromJson(line, GameTickMsgWrapper.class);
            
			if (msgFromServer.msgType.equals("gameInit")) {
                System.out.println("Race init");
                track = new Track(msgFromServer);
                agent = new RacingCarAgent(track, join.getName());
                
                Map<String, OponentAgent> otherCars = new HashMap<String, OponentAgent>();
                
                for (Map car : ((List<Map>)((Map)((Map)msgFromServer.data).get("race")).get("cars"))) {
                	String carName = (String)((Map)car.get("id")).get("name");
                	double carLenght = (double)((Map)car.get("dimensions")).get("length");
                	if (!carName.equals(join.getName())) {
                		otherCars.put(carName, new OponentAgent(track, carName, carLenght));
                	}
                }
                
				
                System.out.println("Track lenght " + track.getTrackLenght());
            } else if (!crashed && msgFromServer.msgType.equals("carPositions")) {
            	Integer gameTick = msgFromServer.gameTick != null ? msgFromServer.gameTick : 0;
            	
            	if (gameTick == null) {
            		System.out.println("Initial positions!");
            	} else if ((previousGameTick != null && gameTick <= previousGameTick) || gameTick == 0.0) {
            		System.out.println("Server >>> out of sync tick! Retrieved tick " + gameTick + " Last tick " + previousGameTick);
            	} else {
	            	System.out.println("Server >>> game tick " + gameTick);
	        		// TODO catch errors in production
	//            	try {
		            	CarPositionsMessageParser carPositionsParser = new CarPositionsMessageParser(msgFromServer);
		            	CarPositionInTrack carPosition = carPositionsParser.createCurrentPosition(track);
		            	
		            	double previousDistance = distanceCalculator.getTotalDistance();
		            	distanceCalculator.add(carPosition.getCurrentPiece(), carPositionsParser.getInPieceDistance());
		            	double currentDistance = distanceCalculator.getTotalDistance();
		            	
						double distanceDifference = currentDistance - previousDistance;
						
						agent.updatePhysics(distanceDifference, (double)(gameTick - (previousGameTick != null ? previousGameTick : 0)), carPosition.getAngle(), carPosition.getPositionTurnSteepness(), carPosition.getCurrentPiece().getLaneAwareRadius(carPosition.getCurrentLane()));
		            	
						
						agent.updateInputData(gameTick, carPosition, null, distanceCalculator); // todo other cars
		            	SendMsg msg = agent.nextAction();
		            	send(msg);
	//            	} catch (Throwable t) {
	//            		send(new Ping());
	//            	}
            	
	            	previousGameTick = gameTick;
            	}
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
            	System.out.println("Turbo available!");
//            	robot.addTurbo(new TurboInfo(turboDurationInTicks, turboFactor));
            } else if (msgFromServer.msgType.equals("crash")) {
            	Object crashedName = ((Map)msgFromServer.data).get("name");
				if (crashedName != null && crashedName.equals(agent.getName())) {
					System.out.println("Agent crashed");
					crashed = true;
					agent.crashed(); // agent crashed - for learning
				}
            } else if (msgFromServer.msgType.equals("spawn")) {
            	Object crashedName = ((Map)msgFromServer.data).get("name");
				if (crashedName != null && crashedName.equals(agent.getName())) {
					System.out.println("Back to the race!");
					crashed = false;
				}
            } else {
            	handleNonRaceServerMessages(msgFromServer);
            }
        }
    }
    
    private void handleNonRaceServerMessages(GameTickMsgWrapper msgFromServer) {
    	if (msgFromServer.msgType.equals("join")) {
            System.out.println("Joined");
        } else if (msgFromServer.msgType.equals("gameEnd")) {
        	ReleaseThrotleQValues.getInstance().printFile();
            System.out.println("Race end");
        } else if (msgFromServer.msgType.equals("gameStart")) {
            System.out.println("Race start");
        } else if (msgFromServer.msgType.equals("dnf")) {
        	System.out.println("Disqualified");
        } else {
            send(new Ping());
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}