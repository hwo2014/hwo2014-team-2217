package noobbot.learning;

public class StateAction {
	
	private State state;
	
	private boolean action;
	
	public StateAction(State state, boolean action) {
	    super();
	    this.state = state;
	    this.action = action;
    }

	public State getState() {
		return state;
	}

	public boolean getAction() {
		return action;
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + (action ? 1231 : 1237);
	    result = prime * result + ((state == null) ? 0 : state.hashCode());
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    StateAction other = (StateAction) obj;
	    if (action != other.action)
		    return false;
	    if (state == null) {
		    if (other.state != null)
			    return false;
	    } else if (!state.equals(other.state))
		    return false;
	    return true;
    }

}
