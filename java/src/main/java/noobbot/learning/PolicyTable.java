package noobbot.learning;


public class PolicyTable {
	
	// higher epsilon higher exploration (that means not choosing greedy action but exploring worse actions too), 
	// because only 2 actions it should be smaller that 0.5 (otherwise it will choose worse action more often)
	// we can start training with 50/50 and later decrease it
	double epsilon = 0.5;

	// learning rate, at first we can set larger number to learn faster and decrease it with time until leave it very small so better solution can converge
	double alpha = 0.35;
	
	// how much weight we put to future states. For this stopping behavior I think this number should be rather high
	double gamma = 0.95;
	
	// double[0] q value for false, double[1] q value for true
	private ReleaseThrotleQValues stateActionQValues = ReleaseThrotleQValues.getInstance();

	public PolicyTable() {
	    super();
    }
	
	// epsilon greedy action selection algorithm
	public boolean selectAction(State state) {
		// Explore vs exploit (depends on epsilon)
		// event to return random action (according to epsilon parameter)
		if (Math.random() < epsilon) {
			return Math.random() < 0.5; // random true or false
		} else {
			return getBestAction(state);
		}
	}
	
	public boolean getBestAction(State state) {

		double[] qValues = stateActionQValues.getQValues(state);
		
		return qValues[1] > qValues[0]; // qValues[1] is for true action so if its larger it will return true
	}
	
	public void updatePreviousQValue(StateAction stateAction, double thisQ, double nextQ, double rewardForPreviousAction) {
		double newQ = thisQ + alpha * (rewardForPreviousAction + gamma * nextQ - thisQ);
		if (stateAction.getAction()) { // if action was true update q values for it
			stateActionQValues.getQValues(stateAction.getState())[1] = newQ;
		} else { // if action was true update q values for it
			stateActionQValues.getQValues(stateAction.getState())[0] = newQ;
		}
	}
	
	public double getQValue(StateAction stateAction) {
		if (stateAction.getAction()) {
			return stateActionQValues.getQValues(stateAction.getState())[1];
		} else {
			return stateActionQValues.getQValues(stateAction.getState())[0];
		}
	}
}
