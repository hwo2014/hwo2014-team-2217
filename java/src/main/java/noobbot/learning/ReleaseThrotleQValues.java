package noobbot.learning;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.google.common.collect.BoundType;
import com.google.common.collect.ImmutableRangeMap;
import com.google.common.collect.Range;
import com.google.common.collect.RangeMap;
import com.google.common.collect.TreeRangeMap;

public class ReleaseThrotleQValues {
	
	private Map<RangeConvertedState, double[]> qValues = new HashMap<ReleaseThrotleQValues.RangeConvertedState, double[]>();
	
	private RangeMap<Double, Range<Double>> velocityRanges = TreeRangeMap.create();
	private RangeMap<Double, Range<Double>> distanceRanges = TreeRangeMap.create();
	private RangeMap<Double, Range<Double>> turnSteepnessRanges = TreeRangeMap.create();
	private RangeMap<Double, Range<Double>> frictionRanges = TreeRangeMap.create();
	private RangeMap<Double, Range<Double>> stoppingPowerRanges = TreeRangeMap.create();
	
	private static ReleaseThrotleQValues instance;
	
	public static ReleaseThrotleQValues getInstance() {
		if (instance == null) {
			instance = new ReleaseThrotleQValues();
		}
		return instance;
	}
	
	@SuppressWarnings("unchecked")
    private ReleaseThrotleQValues() {
	    super();
	    velocityRanges = ImmutableRangeMap.<Double, Range<Double>>builder()
	    		.put(Range.lessThan(5.0), Range.lessThan(5.0))
	    		.put(Range.closedOpen(5.0, 10.0), Range.closedOpen(5.0, 10.0))
	    		.put(Range.closedOpen(10.0, 15.0), Range.closedOpen(10.0, 15.0))
	    		.put(Range.closedOpen(15.0, 20.0), Range.closedOpen(15.0, 20.0))
	    		.put(Range.closedOpen(20.0, 30.0), Range.closedOpen(20.0, 30.0))
	    		.put(Range.closedOpen(30.0, 40.0), Range.closedOpen(30.0, 40.0))
	    		.put(Range.closedOpen(40.0, 50.0), Range.closedOpen(40.0, 50.0))
	    		.put(Range.closedOpen(50.0, 60.0), Range.closedOpen(50.0, 60.0))
	    		.put(Range.closedOpen(60.0, 70.0), Range.closedOpen(60.0, 70.0))
	    		.put(Range.closedOpen(70.0, 80.0), Range.closedOpen(70.0, 80.0))
	    		.put(Range.closedOpen(80.0, 100.0), Range.closedOpen(80.0, 100.0))
	    		.put(Range.closedOpen(100.0, 120.0), Range.closedOpen(100.0, 120.0))
	    		.put(Range.closedOpen(120.0, 140.0), Range.closedOpen(120.0, 140.0))
	    		.put(Range.closedOpen(140.0, 160.0), Range.closedOpen(140.0, 160.0))
	    		.put(Range.closedOpen(160.0, 180.0), Range.closedOpen(160.0, 180.0))
	    		.put(Range.closedOpen(180.0, 200.0), Range.closedOpen(180.0, 200.0))
	    		.put(Range.closedOpen(200.0, 230.0), Range.closedOpen(200.0, 230.0))
	    		.put(Range.closedOpen(230.0, 260.0), Range.closedOpen(230.0, 260.0))
	    		.put(Range.closedOpen(260.0, 300.0), Range.closedOpen(260.0, 300.0))
	    		.put(Range.downTo(300.00, BoundType.CLOSED), Range.downTo(300.00, BoundType.CLOSED))
	    		.build();
	    
	    distanceRanges = ImmutableRangeMap.<Double, Range<Double>>builder()
	    		.put(Range.lessThan(10.0), Range.lessThan(10.0))
	    		.put(Range.closedOpen(10.0, 20.0), Range.closedOpen(10.0, 20.0))
	    		.put(Range.closedOpen(20.0, 30.0), Range.closedOpen(20.0, 30.0))
	    		.put(Range.closedOpen(30.0, 40.0), Range.closedOpen(30.0, 40.0))
	    		.put(Range.closedOpen(40.0, 50.0), Range.closedOpen(40.0, 50.0))
	    		.put(Range.closedOpen(50.0, 60.0), Range.closedOpen(50.0, 60.0))
	    		.put(Range.closedOpen(60.0, 70.0), Range.closedOpen(60.0, 70.0))
	    		.put(Range.closedOpen(70.0, 80.0), Range.closedOpen(70.0, 80.0))
	    		.put(Range.closedOpen(80.0, 100.0), Range.closedOpen(80.0, 100.0))
	    		.put(Range.closedOpen(100.0, 120.0), Range.closedOpen(100.0, 120.0))
	    		.put(Range.closedOpen(120.0, 140.0), Range.closedOpen(120.0, 140.0))
	    		.put(Range.closedOpen(140.0, 160.0), Range.closedOpen(140.0, 160.0))
	    		.put(Range.closedOpen(160.0, 180.0), Range.closedOpen(160.0, 180.0))
	    		.put(Range.closedOpen(180.0, 200.0), Range.closedOpen(180.0, 200.0))
	    		.put(Range.closedOpen(200.0, 230.0), Range.closedOpen(200.0, 230.0))
	    		.put(Range.closedOpen(230.0, 260.0), Range.closedOpen(230.0, 260.0))
	    		.put(Range.closedOpen(260.0, 300.0), Range.closedOpen(260.0, 300.0))
	    		.put(Range.closedOpen(300.0, 350.0), Range.closedOpen(300.0, 350.0))
	    		.put(Range.closedOpen(350.0, 400.0), Range.closedOpen(350.0, 400.0))
	    		.put(Range.closedOpen(400.0, 500.0), Range.closedOpen(400.0, 500.0))
	    		.put(Range.downTo(500.00, BoundType.CLOSED), Range.downTo(500.00, BoundType.CLOSED))
	    		.build();

	    turnSteepnessRanges = ImmutableRangeMap.<Double, Range<Double>>builder()
	    		.put(Range.lessThan(0.0), Range.lessThan(0.0))
	    		.put(Range.closedOpen(0.0, 0.025), Range.closedOpen(0.0, 0.025))
	    		.put(Range.closedOpen(0.025, 0.05), Range.closedOpen(0.025, 0.05))
	    		.put(Range.closedOpen(0.05, 0.075), Range.closedOpen(0.05, 0.075))
	    		.put(Range.closedOpen(0.075, 0.1), Range.closedOpen(0.075, 0.1))
	    		.put(Range.closedOpen(0.1, 0.15), Range.closedOpen(0.1, 0.15))
	    		.put(Range.closedOpen(0.15, 0.2), Range.closedOpen(0.15, 0.2))
	    		.put(Range.closedOpen(0.2, 0.25), Range.closedOpen(0.2, 0.25))
	    		.put(Range.closedOpen(0.25, 0.3), Range.closedOpen(0.25, 0.3))
	    		.put(Range.closedOpen(0.3, 0.4), Range.closedOpen(0.3, 0.4))
	    		.put(Range.closedOpen(0.4, 0.5), Range.closedOpen(0.4, 0.5))
	    		.put(Range.closedOpen(0.5, 0.7), Range.closedOpen(0.5, 0.7))
	    		.put(Range.closedOpen(0.7, 1.0), Range.closedOpen(0.7, 1.0))
	    		.put(Range.downTo(1.0, BoundType.CLOSED), Range.downTo(1.0, BoundType.CLOSED))
	    		.build();

	    frictionRanges = ImmutableRangeMap.<Double, Range<Double>>builder()
	    		.put(Range.lessThan(0.0), Range.lessThan(0.0))
	    		.put(Range.closedOpen(0.0, 0.05), Range.closedOpen(0.0, 0.05))
	    		.put(Range.closedOpen(0.05, 0.1), Range.closedOpen(0.05, 0.1))
	    		.put(Range.closedOpen(0.1, 0.15), Range.closedOpen(0.1, 0.15))
	    		.put(Range.closedOpen(0.15, 0.2), Range.closedOpen(0.15, 0.2))
	    		.put(Range.closedOpen(0.2, 0.25), Range.closedOpen(0.2, 0.25))
	    		.put(Range.closedOpen(0.25, 0.3), Range.closedOpen(0.25, 0.3))
	    		.put(Range.closedOpen(0.3, 0.4), Range.closedOpen(0.3, 0.4))
	    		.put(Range.closedOpen(0.4, 0.5), Range.closedOpen(0.4, 0.5))
	    		.put(Range.closedOpen(0.5, 0.7), Range.closedOpen(0.5, 0.7))
	    		.put(Range.closedOpen(0.7, 1.0), Range.closedOpen(0.7, 1.0))
	    		.put(Range.downTo(1.0, BoundType.CLOSED), Range.downTo(1.0, BoundType.CLOSED))
	    		.build();
	    
	    stoppingPowerRanges = ImmutableRangeMap.<Double, Range<Double>>builder()
	    		.put(Range.lessThan(0.1), Range.lessThan(0.1))
	    		.put(Range.closedOpen(0.1, 0.25), Range.closedOpen(0.1, 0.25))
	    		.put(Range.closedOpen(0.25, 0.5), Range.closedOpen(0.25, 0.5))
	    		.put(Range.closedOpen(0.5, 0.75), Range.closedOpen(0.5, 0.75))
	    		.put(Range.closedOpen(0.75, 1.0), Range.closedOpen(0.75, 1.0))
	    		.put(Range.closedOpen(1.0, 1.5), Range.closedOpen(1.0, 1.5))
	    		.put(Range.closedOpen(1.5, 2.0), Range.closedOpen(1.5, 2.0))
	    		.put(Range.closedOpen(2.0, 3.0), Range.closedOpen(2.0, 3.0))
	    		.put(Range.downTo(3.0, BoundType.CLOSED), Range.downTo(10.00, BoundType.CLOSED))
	    		.build();
	    
	    File initQValuesFiles = new File("releaseThrotleQValues.txt");
	    if (initQValuesFiles.exists()) {
//	    	BufferedReader reader = null;
//	    	try {
//		    	reader = new BufferedReader(new FileReader(initQValuesFiles));
//		    	String line = "";
//	    	
//	            while ((line = reader.readLine()) != null) {
//	            	String[] elements = line.split(" ");
//	            	Converter<Double, String> doubleConverter = Doubles.stringConverter().reverse();
//					Converter<String, Range<Double>> rangeConverter = GuavaRangeStringConverter.rangeConverter(doubleConverter).reverse();
//					Range<Double> velocityRange = rangeConverter.convert(elements[0]);
//					Range<Double> distanceToNextTurnRange = rangeConverter.convert(elements[1]);
//					Range<Double> nextTurnSteepnessRange = rangeConverter.convert(elements[2]);
//					Range<Double> frictionRange = rangeConverter.convert(elements[3]);
//					Range<Double> stoppingPowerRange = rangeConverter.convert(elements[4]);
//					Double trueQValue = Doubles.stringConverter().convert(elements[8]);
//					Double falseQValue = Doubles.stringConverter().convert(elements[10]);
//	            	qValues.put(new RangeConvertedState(velocityRange, distanceToNextTurnRange, nextTurnSteepnessRange, frictionRange, stoppingPowerRange), new double[] {falseQValue, trueQValue});
//	            }
	    		
	    		ObjectInputStream in = null;
                try {
	                in = new ObjectInputStream(new FileInputStream(initQValuesFiles));
	                System.out.println("Loading data...");
	                qValues = (Map<ReleaseThrotleQValues.RangeConvertedState, double[]>)in.readObject();
	                System.out.println("Data loaded");
                } catch (ClassNotFoundException | IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
                } finally {
                	try {
                		in.close();
                	} catch (IOException e) {
                		// TODO Auto-generated catch block
                		e.printStackTrace();
                	}
                }
//            } catch (IOException e) {
//            	// todo try if not error in CI
//	            e.printStackTrace();
//	            // initialize all q values
//			    for (Range<Double> velocityRange : velocityRanges.asMapOfRanges().values())
//			    	for (Range<Double> distanceRange : distanceRanges.asMapOfRanges().values())
//			    		for (Range<Double> steepnessRange : turnSteepnessRanges.asMapOfRanges().values())
//			    			for (Range<Double> frictionRange : frictionRanges.asMapOfRanges().values())
//			    				for (Range<Double> stoppingPowerRange : stoppingPowerRanges.asMapOfRanges().values()) {
//			    					qValues.put(new RangeConvertedState(velocityRange, distanceRange, 
//			    							steepnessRange, frictionRange, stoppingPowerRange), new double[]{0, 0});
//			    				}
//            } finally {
//            	try {
//	                reader.close();
//                } catch (IOException e) {
//	                // TODO Auto-generated catch block
//	                e.printStackTrace();
//                }
//            }
//	    	
	    } else {
	    
		    // initialize all q values
		    for (Range<Double> velocityRange : velocityRanges.asMapOfRanges().values())
		    	for (Range<Double> distanceRange : distanceRanges.asMapOfRanges().values())
		    		for (Range<Double> steepnessRange : turnSteepnessRanges.asMapOfRanges().values())
		    			for (Range<Double> frictionRange : frictionRanges.asMapOfRanges().values())
		    				for (Range<Double> stoppingPowerRange : stoppingPowerRanges.asMapOfRanges().values()) {
		    					qValues.put(new RangeConvertedState(velocityRange, distanceRange, 
		    							steepnessRange, frictionRange, stoppingPowerRange), new double[]{0, 0});
		    				}
	    }
    }

	
	public double[] getQValues(State state) {
		RangeConvertedState convertToRangedState = convertToRangedState(state);
		double[] ds = qValues.get(convertToRangedState);
		return ds;
	}
	
	public RangeConvertedState convertToRangedState(State state) {
		RangeConvertedState rangeConvertedState = new RangeConvertedState(velocityRanges.get(state.getCurrentVelocity()),
				distanceRanges.get(state.getDistanceToNextTurn()),
				turnSteepnessRanges.get(state.getNextTurnSteepness()),
				frictionRanges.get(state.getFriction()),
				stoppingPowerRanges.get(state.getStoppingPower()));
		return rangeConvertedState;
	}

	static class RangeConvertedState implements Serializable {
        private static final long serialVersionUID = 1L;
        
		private Range<Double> velocityRange;
		private Range<Double> distanceToNextTurnRange;
		private Range<Double> nextTurnSteepnessRange;
		private Range<Double> frictionRange; 
		private Range<Double> stoppingPowerRange;
		public RangeConvertedState(Range<Double> velocityRange, Range<Double> distanceToNextTurnRange,
                Range<Double> nextTurnSteepnessRange, Range<Double> frictionRange, Range<Double> stoppingPowerRange) {
	        super();
	        this.velocityRange = velocityRange;
	        this.distanceToNextTurnRange = distanceToNextTurnRange;
	        this.nextTurnSteepnessRange = nextTurnSteepnessRange;
	        this.frictionRange = frictionRange;
	        this.stoppingPowerRange = stoppingPowerRange;
        }

		@Override
        public int hashCode() {
	        final int prime = 31;
	        int result = 1;
	        result = prime * result + ((distanceToNextTurnRange == null) ? 0 : distanceToNextTurnRange.hashCode());
	        result = prime * result + ((frictionRange == null) ? 0 : frictionRange.hashCode());
	        result = prime * result + ((nextTurnSteepnessRange == null) ? 0 : nextTurnSteepnessRange.hashCode());
	        result = prime * result + ((stoppingPowerRange == null) ? 0 : stoppingPowerRange.hashCode());
	        result = prime * result + ((velocityRange == null) ? 0 : velocityRange.hashCode());
	        return result;
        }
		@Override
        public boolean equals(Object obj) {
	        if (this == obj)
		        return true;
	        if (obj == null)
		        return false;
	        if (getClass() != obj.getClass())
		        return false;
	        RangeConvertedState other = (RangeConvertedState) obj;
	        if (distanceToNextTurnRange == null) {
		        if (other.distanceToNextTurnRange != null)
			        return false;
	        } else if (!distanceToNextTurnRange.equals(other.distanceToNextTurnRange))
		        return false;
	        if (frictionRange == null) {
		        if (other.frictionRange != null)
			        return false;
	        } else if (!frictionRange.equals(other.frictionRange))
		        return false;
	        if (nextTurnSteepnessRange == null) {
		        if (other.nextTurnSteepnessRange != null)
			        return false;
	        } else if (!nextTurnSteepnessRange.equals(other.nextTurnSteepnessRange))
		        return false;
	        if (stoppingPowerRange == null) {
		        if (other.stoppingPowerRange != null)
			        return false;
	        } else if (!stoppingPowerRange.equals(other.stoppingPowerRange))
		        return false;
	        if (velocityRange == null) {
		        if (other.velocityRange != null)
			        return false;
	        } else if (!velocityRange.equals(other.velocityRange))
		        return false;
	        return true;
        }
	}

    public void printFile() {
//    	BufferedWriter writer = null;
//    	try {
//	        writer = new BufferedWriter(new FileWriter(new File("releaseThrotleQValues.txt")));
//	        
//	        Converter<Double, String> doubleConverter = Doubles.stringConverter().reverse();
//			Converter<Range<Double>, String> rangeConverter = GuavaRangeStringConverter.rangeConverter(doubleConverter);
//	        
//	        for (Range<Double> velocityRange : velocityRanges.asMapOfRanges().values())
//		    	for (Range<Double> distanceRange : distanceRanges.asMapOfRanges().values())
//		    		for (Range<Double> steepnessRange : turnSteepnessRanges.asMapOfRanges().values())
//		    			for (Range<Double> frictionRange : frictionRanges.asMapOfRanges().values())
//		    				for (Range<Double> stoppingPowerRange : stoppingPowerRanges.asMapOfRanges().values()) {
//	        					double[] values = qValues.get(new RangeConvertedState(velocityRange, distanceRange, 
//	        							steepnessRange, frictionRange, stoppingPowerRange));
//	        					if (values[0] != 0.0 || values[1] != 0.0) {
//		        					writer.append(rangeConverter.convert(velocityRange) + " " + rangeConverter.convert(distanceRange) + " " +
//		        							rangeConverter.convert(steepnessRange) + " " + rangeConverter.convert(frictionRange) + " " + rangeConverter.convert(stoppingPowerRange) + " " +
//		        							" Q values: true " + values[1] + " false " + values[0]);
//		        					writer.newLine();
//	        					}
//	        				}
//        } catch (IOException e) {
//	        e.printStackTrace();
//        } finally {
//        	if (writer != null) {
//        		try {
//	                writer.close();
//                } catch (IOException e) {
//	                e.printStackTrace();
//                }
//        	}
//        }
    	
    	try {
	        File qValuesFile = new File("releaseThrotleQValues.txt");
	        if (qValuesFile.exists()) {
	        	qValuesFile.delete();
	        }
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(qValuesFile));
	        out.writeObject(qValues);
	        out.close();
        } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
        }
    }
	
}

