package noobbot.learning;

public class State {
	
	private double nextTurnSteepness;
	
	private double distanceToNextTurn;
	
	private double currentVelocity;
	
	private double stoppingPower;
	
	private double friction;

	public State(double nextTurnSteepness, double distanceToNextTurn, double currentSpeed, double stoppingPower, double friction) {
	    super();
	    this.distanceToNextTurn = distanceToNextTurn > 0 ? distanceToNextTurn : 0;
	    this.currentVelocity = currentSpeed;
	    this.stoppingPower = stoppingPower;
	    this.nextTurnSteepness = nextTurnSteepness;
	    this.friction = friction;
    }

	public double getDistanceToNextTurn() {
		return distanceToNextTurn;
	}

	public double getCurrentVelocity() {
		return currentVelocity;
	}

	public double getStoppingPower() {
		return stoppingPower;
	}
	
	public double getNextTurnSteepness() {
		return nextTurnSteepness;
	}

	public double getFriction() {
		return friction;
	}

	@Override
    public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    long temp;
	    temp = Double.doubleToLongBits(currentVelocity);
	    result = prime * result + (int) (temp ^ (temp >>> 32));
	    temp = Double.doubleToLongBits(distanceToNextTurn);
	    result = prime * result + (int) (temp ^ (temp >>> 32));
	    temp = Double.doubleToLongBits(friction);
	    result = prime * result + (int) (temp ^ (temp >>> 32));
	    temp = Double.doubleToLongBits(nextTurnSteepness);
	    result = prime * result + (int) (temp ^ (temp >>> 32));
	    temp = Double.doubleToLongBits(stoppingPower);
	    result = prime * result + (int) (temp ^ (temp >>> 32));
	    return result;
    }

	@Override
    public boolean equals(Object obj) {
	    if (this == obj)
		    return true;
	    if (obj == null)
		    return false;
	    if (getClass() != obj.getClass())
		    return false;
	    State other = (State) obj;
	    if (Double.doubleToLongBits(currentVelocity) != Double.doubleToLongBits(other.currentVelocity))
		    return false;
	    if (Double.doubleToLongBits(distanceToNextTurn) != Double.doubleToLongBits(other.distanceToNextTurn))
		    return false;
	    if (Double.doubleToLongBits(friction) != Double.doubleToLongBits(other.friction))
		    return false;
	    if (Double.doubleToLongBits(nextTurnSteepness) != Double.doubleToLongBits(other.nextTurnSteepness))
		    return false;
	    if (Double.doubleToLongBits(stoppingPower) != Double.doubleToLongBits(other.stoppingPower))
		    return false;
	    return true;
    }

}
