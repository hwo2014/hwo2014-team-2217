package noobbot.learning;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.physics.Physics;
import noobbot.track.Lane;
import noobbot.track.Track;
import noobbot.track.TrackPiece;

public class World {
	
	private Physics physics;
	private CarPositionInTrack position;
	@SuppressWarnings("unused")
    private List<CarPositionInTrack> otherCarsInTrack;
	private Track track;
	private boolean crashed;
	
	public World(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, boolean crashed) {
	    super();
	    this.physics = physics;
	    this.position = position;
	    this.otherCarsInTrack = otherCarsInTrack;
	    this.track = track;
	    this.crashed = crashed;
    }

	public State getState() {
		TrackPiece currentPiece = position.getCurrentPiece();
		Lane currentLane = position.getCurrentLane();
		
		if (currentPiece.getAngle() == null || currentPiece.getAngle() == 0.0) {
			double distanceInPiece = position.getPositionInPiece();
			
			double distanceToNextTurn = currentPiece.getLength() - distanceInPiece; // init distance if next piece is turn
			TrackPiece tempCurrentPiece = currentPiece;
			while (true) {
				TrackPiece nextPiece = tempCurrentPiece.getNext();
				if (nextPiece.getAngle() != null && nextPiece.getAngle() != 0.0) {
					return new State(nextPiece.getTurnSteepness(), distanceToNextTurn, physics.getVelocity(), physics.getStoppingPower(), physics.getFriction());
				} else {
					distanceToNextTurn += nextPiece.getLength();
				}
				if (distanceToNextTurn > track.getTrackLenght()) {
					// in case of programming error in building track or smth
					return new State(0.1, 100, physics.getVelocity(), physics.getStoppingPower(), physics.getFriction()); // todo check real radiuses
				}
				tempCurrentPiece = nextPiece;
			}
		} else {
			double nextTurnRadiusWithLane = currentPiece.getRadius() + currentLane.getDistanceFromCenter();
			return new State(currentPiece.getAngle(), nextTurnRadiusWithLane, 0, physics.getVelocity(), physics.getStoppingPower());
		}
	}
	
	public double getRewardForPreviousAction(boolean previouslySelectedAction) {
		if (previouslySelectedAction == false && crashed) {
			System.out.println("LEARNING FRAMEWORK >>> reward: " + -70.0);
			return -70.0;
		} else if (previouslySelectedAction == true && crashed) { // if crashed even for true action (meaning stopping) then set even higher punishment so action before would be punished even more
			System.out.println("LEARNING FRAMEWORK >>> reward: " + -120.0);
			return -120.0;
		}
		double velocityDiffReward = getState().getCurrentVelocity() - physics.getAverageVelocity();
		System.out.println("LEARNING FRAMEWORK >>> reward: " + velocityDiffReward);
		return velocityDiffReward;
	}

	public State getEstimatedNextState(State currentState, boolean selectedAction) {
		if (selectedAction == true) {
			double distanceToNextTurn = currentState.getDistanceToNextTurn() - currentState.getCurrentVelocity();
			return new State(currentState.getNextTurnSteepness(), distanceToNextTurn, physics.getVelocity(), physics.getStoppingPower(), physics.getFriction());
		} else {
			double distanceToNextTurn = currentState.getDistanceToNextTurn() - currentState.getCurrentVelocity();
			return new State(currentState.getNextTurnSteepness(), distanceToNextTurn, physics.getVelocity(), physics.getStoppingPower(), physics.getFriction());
		}
	}

}
