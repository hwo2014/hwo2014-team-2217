package noobbot.learning;

import java.util.ArrayList;

import com.google.common.base.Converter;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;
import com.google.common.collect.BoundType;
import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.collect.RangeSet;

public class GuavaRangeStringConverter {
	
	/**
	 * Converter between Range instances and Strings, essentially a custom serializer.
	 * Ideally we'd let Gson or Guava do this for us, but presently this is cleaner.
	 */
	public static <T extends Comparable<? super T>> Converter<Range<T>, String> rangeConverter(final Converter<T, String> elementConverter) {
	  final String NEG_INFINITY = "-NAN";
	  final String POS_INFINITY = "NAN";
	  final String DOTDOT = "\u2025";
	  return new Converter<Range<T>, String>() {
	    @Override
	    protected String doForward(Range<T> range) {
	      return (range.hasLowerBound() && range.lowerBoundType() == BoundType.CLOSED ? "[" : "(") +
	             (range.hasLowerBound() ? elementConverter.convert(range.lowerEndpoint()) : NEG_INFINITY) +
	             DOTDOT +
	             (range.hasUpperBound() ? elementConverter.convert(range.upperEndpoint()) : POS_INFINITY) +
	             (range.hasUpperBound() && range.upperBoundType() == BoundType.CLOSED ? "]" : ")");
	    }

	    @Override
	    protected Range<T> doBackward(String range) {
	      String[] endpoints = range.split(DOTDOT);

	      Range<T> ret = Range.all();
	      if(!endpoints[0].substring(1).equals(NEG_INFINITY)) {
	        T lower = elementConverter.reverse().convert(endpoints[0].substring(1));
	        ret = ret.intersection(Range.downTo(lower, endpoints[0].charAt(0) == '[' ? BoundType.CLOSED : BoundType.OPEN));
	      }
	      if(!endpoints[1].substring(0,endpoints[1].length()-1).equals(POS_INFINITY)) {
	        T upper = elementConverter.reverse().convert(endpoints[1].substring(0,endpoints[1].length()-1));
	        ret = ret.intersection(Range.upTo(upper, endpoints[1].charAt(endpoints[1].length()-1) == ']' ? BoundType.CLOSED : BoundType.OPEN));
	      }
	      return ret;
	    }
	  };
	}

	/**
	 * Converter between RangeSet instances and Strings, essentially a custom serializer.
	 * Ideally we'd let Gson or Guava do this for us, but presently this is cleaner.
	 */
	public static <T extends Comparable<? super T>> Converter<RangeSet<T>, String> rangeSetConverter(final Converter<T, String> elementConverter) {
	  return new Converter<RangeSet<T>, String>() {
	    private final Converter<Range<T>, String> rangeConverter = rangeConverter(elementConverter);
	    @Override
	    protected String doForward(RangeSet<T> rs) {
	      ArrayList<String> ls = new ArrayList<>();
	      for(Range<T> range : rs.asRanges()) {
	        ls.add(rangeConverter.convert(range));
	      }
	      return Joiner.on(", ").join(ls);
	    }

	    @Override
	    protected RangeSet<T> doBackward(String rs) {
	      Iterable<String> parts = Splitter.on(",").trimResults().split(rs);
	      ImmutableRangeSet.Builder<T> build = ImmutableRangeSet.builder();
	      for(String range : parts) {
	        build.add(rangeConverter.reverse().convert(range));
	      }
	      return build.build();
	    }
	  };
	}

}
