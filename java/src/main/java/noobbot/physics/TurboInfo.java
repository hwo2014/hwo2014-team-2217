package noobbot.physics;

public class TurboInfo {
	
	private int turboDurationInTicks;
	
	private double turboFactor;
	
	public TurboInfo(int turboDurationInTicks, double turboFactor) {
	    super();
	    this.turboDurationInTicks = turboDurationInTicks;
	    this.turboFactor = turboFactor;
    }

	public int getTurboDurationInTicks() {
		return turboDurationInTicks;
	}

	public double getTurboFactor() {
		return turboFactor;
	}

}
