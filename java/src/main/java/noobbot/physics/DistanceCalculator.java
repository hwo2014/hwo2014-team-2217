package noobbot.physics;

import noobbot.track.TrackPiece;

public class DistanceCalculator {
	
	private double totalDistance = 0;
	
	private TrackPiece currentTrackPiece;

	private double previousDistanceInPiece;
	
	public void add(TrackPiece currentPiece, double distanceInPiece) {
		if (currentTrackPiece == null) {
			currentTrackPiece = currentPiece;
		}
		if (currentTrackPiece.getId() == currentPiece.getId()) {
			totalDistance += (distanceInPiece - previousDistanceInPiece);
		} else {
			totalDistance += (currentTrackPiece.getLength() - previousDistanceInPiece + distanceInPiece);
			currentTrackPiece = currentPiece;
		}
		previousDistanceInPiece = distanceInPiece;
	}

	public double getTotalDistance() {
		return totalDistance;
	}

}
