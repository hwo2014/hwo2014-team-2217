package noobbot.physics;


public class Physics {
	
	private double velocity = 0.0;

	private double previousVelocity = 0.0;
	
	private double averageVelocity = 0.0;

	private double acceleration = 0.0;
	
	private double friction = 0.0;
	
	private double accelerationPower = 0.0;
	
	private double stoppingPower = 0.0;
	
	private int i = 0;
	
	public void updatePhysics(double previousThrotleDifference, double distance, double time, double carAngle, double turnSteepness, double laneAwareRadius) {
		this.previousVelocity = this.velocity;
		if (distance > 0) {
			this.velocity = distance / time;
		}
		this.averageVelocity = (this.averageVelocity * i + this.velocity) / (i +1);
		i++;
	    this.acceleration = (velocity - previousVelocity) / time;
	    if (friction == 0.0 && laneAwareRadius != 0.0 && carAngle != 0.0) {
	    	double b = velocity / laneAwareRadius * 180 / Math.PI;
	    	double bslip = b - carAngle;
	    	double thresholdSpeed = bslip * Math.PI / 180 * laneAwareRadius;
	    	this.friction = thresholdSpeed * thresholdSpeed / laneAwareRadius;
	    }
    	if (previousThrotleDifference > 0 && acceleration > 0) {
	    	this.accelerationPower = (previousThrotleDifference * acceleration);
	    }
	    if (previousThrotleDifference < 0 && acceleration < 0) {
	    	this.stoppingPower = (previousThrotleDifference * acceleration);
	    }
	    
    }

	public double getVelocity() {
		return velocity;
	}

	public double getAcceleration() {
		return acceleration;
	}

	public Double getFriction() {
		return friction;
	}

	public double getAccelerationPower() {
		return accelerationPower;
	}

	public double getStoppingPower() {
		return stoppingPower;
	}
	
	public Double getPreviousVelocity() {
		return previousVelocity;
	}
	
	public Double getAverageVelocity() {
		return averageVelocity;
	}

	public void addTurbo(TurboInfo turbo) {
		accelerationPower = accelerationPower * turbo.getTurboFactor();
	}
}
