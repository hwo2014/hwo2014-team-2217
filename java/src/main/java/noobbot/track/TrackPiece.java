package noobbot.track;

public class TrackPiece {

    public enum Type {
        STRAIGHT, LEFT, RIGHT
    }
    
    private final double id;

    private final Type type;

    private final Double length;

    private final Double angle;

    private final Double radius;

    private final boolean hasSwitch;

    private TrackPiece next;

    public TrackPiece(double id, Double length, boolean hasSwitch) {
    	this.id = id;
        this.type = Type.STRAIGHT;
        this.length = length;
        this.angle = null;
        this.radius = 0.0;
        this.hasSwitch = hasSwitch;
    }

    public TrackPiece(double id, Double angle, Double radius, boolean hasSwitch) {
    	this.id = id;
        this.type = angle > 0 ? Type.RIGHT : angle < 0 ? Type.LEFT : Type.STRAIGHT;
        this.angle = angle;
        this.radius = radius;
        this.length = (2 * Math.PI * radius * Math.abs(angle)) / 360;
        this.hasSwitch = hasSwitch;
    }

    public Type getType() {
        return type;
    }

    public Double getLength() {
        return length;
    }

    public Double getAngle() {
        return angle;
    }

    public Double getRadius() {
        return radius;
    }
    
    public Double getLaneAwareRadius(Lane lane) {
        return radius + lane.getDistanceFromCenter();
    }

    public boolean isHasSwitch() {
        return hasSwitch;
    }
    
	public double getId() {
		return id;
	}

	public TrackPiece getNext() {
		return next;
	}

	// not nice but will be ok :)
	public void setNext(TrackPiece next) {
		this.next = next;
	}

	public double getTurnSteepness() {
		if (angle != null && angle != 0) {
			double totalAngle = angle;
			TrackPiece next = getNext();
			while (next.angle != null && next.angle != 0.0 && ((angle < 0 && next.angle < 0) || (angle > 0 && next.angle > 0))) {
				totalAngle += next.angle;
				next = next.getNext();
			}
			return Math.abs((10 / radius) * (totalAngle / 180));
			// the lower the radius, and the higher the angle the more steep turn is. for radius 10 and turn 180 its very steep turnaround
		}
		return 0.0;
	}
}
