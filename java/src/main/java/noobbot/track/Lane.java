package noobbot.track;

public class Lane {
	
	private double laneIndex;
	
	private double distanceFromCenter;

	public Lane(double laneIndex, double distanceFromCenter) {
	    super();
	    this.laneIndex = laneIndex;
	    this.distanceFromCenter = distanceFromCenter;
    }

	public double getLaneIndex() {
		return laneIndex;
	}

	public double getDistanceFromCenter() {
		return distanceFromCenter;
	}
	
}
