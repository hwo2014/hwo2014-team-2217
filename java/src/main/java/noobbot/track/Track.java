package noobbot.track;

import java.util.List;
import java.util.Map;

import noobbot.commands.GameTickMsgWrapper;

import com.google.common.collect.Maps;
import com.google.gson.internal.LinkedTreeMap;

public class Track {
	
	private String id;
	
	private String name;
	
	private double trackLenght = 0;
	
    private final Map<Double, TrackPiece> pieces = Maps.newLinkedHashMap();

    private final Map<Double, Lane> lanes = Maps.newLinkedHashMap();
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Track(GameTickMsgWrapper initMessage) {
	    super();
	    Map trackInfo = (Map)((Map)((Map)initMessage.data).get("race")).get("track");
	    id = (String)trackInfo.get("id");
	    name = (String)trackInfo.get("name");
	    List<LinkedTreeMap> piecesInfoList = (List<LinkedTreeMap>)trackInfo.get("pieces");
	    List<LinkedTreeMap> lanesInfoList = (List<LinkedTreeMap>)trackInfo.get("lanes");
	    
	    for (LinkedTreeMap laneInfo : lanesInfoList) {
	    	double laneIndex = (double)laneInfo.get("index");
			lanes.put(laneIndex, new Lane(laneIndex, (double)laneInfo.get("distanceFromCenter")));
	    }
	    
	    Integer trackPieceIndex = 0;
	    for (LinkedTreeMap pieceInfo : piecesInfoList) {
	    	
	    	Double length = pieceInfo.get("length") != null ? (Double)pieceInfo.get("length") : null;
	    	Double angle = pieceInfo.get("angle") != null ? (Double)pieceInfo.get("angle") : null;
	    	Double radius = pieceInfo.get("radius") != null ? (Double)pieceInfo.get("radius") : null;
	    	boolean hasSwitch = pieceInfo.get("switch") != null ? (boolean)pieceInfo.get("switch") : false;
	    	if (length != null) {
	    		TrackPiece trackPiece = new TrackPiece(trackPieceIndex.doubleValue(), length, hasSwitch);
	    		addPiece(trackPiece);
	    	} else if (angle != null && radius != null) {
	    		TrackPiece trackPiece = new TrackPiece(trackPieceIndex.doubleValue(), angle, radius, hasSwitch);
	    		addPiece(trackPiece);
	    	} else {
	    		System.out.println("!!!!!!!!!!!!!!!!! ERROR BUILDING TRACK INFO !!!!!!!!!!!!!!!!");
	    	}
	    	trackPieceIndex++;
	    }
	    
	    // set next links
	    // TODO sometimes nullpointer exception!!!
	    TrackPiece lastTrackPiece = pieces.get(0.0);
	    for (Integer i = pieces.size() - 1; i >= 0; i--) {
	    	TrackPiece piece = pieces.get(i.doubleValue());
	    	piece.setNext(lastTrackPiece);
	    	lastTrackPiece = piece;
	    }
	    
	    for (TrackPiece piece : pieces.values()) {
	    	System.out.println("Piece " + piece.getId() + " lenght " + piece.getLength() + " angle " + piece.getAngle() + " radius " + piece.getRadius() + " turn steep " + piece.getTurnSteepness());
	    }
	    
    }
    
    // for testing
    public Track(List<TrackPiece> trackPieces, List<Lane> trackLanes, double lenght) {
    	this.trackLenght = lenght;
    	for (TrackPiece piece : trackPieces) {
    		pieces.put(piece.getId(), piece);
    	}
    	for (Lane lane : trackLanes) {
    		lanes.put(lane.getLaneIndex(), lane);
    	}
    }

	public TrackPiece getTrackPieceById(double trackPiece) {
		try {
			return pieces.get(trackPiece);
		} catch (Throwable t) {
			System.out.println("!!!!!!!!!!!!!!!! CANNOT GET TRACK PIECE BY ID !!!!!!!!!!!!!!!!");
			return pieces.get(0.0);
		}
    }
	
	public double getMaxLaneId() {
		return lanes.values().stream().max((Lane l1, Lane l2) -> (l1.getLaneIndex() >= l2.getLaneIndex()) ? 1 : 0).get().getLaneIndex();
	}
	
	public Lane getLaneById(double laneId) {
		try {
			return lanes.get(laneId);
		} catch (Throwable t) {
			System.out.println("!!!!!!!!!!!!!!!! CANNOT GET LANE !!!!!!!!!!!!!!!!");
			return lanes.get(0);
		}
	}
	
	private void addPiece(TrackPiece piece) {
		trackLenght += piece.getLength();
    	pieces.put(piece.getId(), piece);
    }

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public double getTrackLenght() {
		return trackLenght;
	}

}
