package noobbot.json;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RaceDefinition {

    public Race race;

    public static class Race {
        public Track track;
    }

    public static class Track {
        public String id;
        public String name;
        public List<Piece> pieces;
    }

    public static class Piece {
        public Double length;
        @SerializedName("switch")
        public boolean hasSwitch;
        public Double angle;
        public Double radius;
    }
}
