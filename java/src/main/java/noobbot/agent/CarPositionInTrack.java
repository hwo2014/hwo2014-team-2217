package noobbot.agent;

import noobbot.track.Lane;
import noobbot.track.Track;
import noobbot.track.TrackPiece;

public class CarPositionInTrack {
	
	private Track track;

	private double lane;

	private double currentPiece;

	private double positionInPiece;

	private double angle;

	private double lap;

	public CarPositionInTrack(Track track, double lane, double currentPiece, double positionInPiece, double angle, double lap) {
		super();
		this.track = track;
		this.lane = lane;
		this.currentPiece = currentPiece;
		this.positionInPiece = positionInPiece;
		this.angle = angle;
		this.lap = lap;
	}

	public double getLaneId() {
		return lane;
	}

	public double getCurrentPieceId() {
		return currentPiece;
	}

	public double getPositionInPiece() {
		return positionInPiece;
	}

	public double getAngle() {
		return angle;
	}

	public double getLap() {
		return lap;
	}

	public TrackPiece getCurrentPiece() {
		return track.getTrackPieceById(currentPiece);
	}
	
	public Lane getCurrentLane() {
		return track.getLaneById(lane);
	}
	
	public double getPositionTurnSteepness() {
		TrackPiece piece = getCurrentPiece();
		if (piece.getAngle() != null && piece.getAngle() != 0) {
			Lane lane = getCurrentLane();
			double turnRadius = piece.getRadius() + lane.getDistanceFromCenter();
			double turnAngle = piece.getAngle();
			
			double totalAngle = turnAngle;
			TrackPiece next = piece.getNext();
			while (next.getAngle() != null && next.getAngle() != 0.0 && ((turnAngle < 0 && next.getAngle() < 0) || (turnAngle > 0 && next.getAngle() > 0))) {
				totalAngle += next.getAngle();
				next = next.getNext();
			}
			return Math.abs((10 / turnRadius) * (totalAngle / 180));
			// the lower the radius, and the higher the angle the more steep turn is. for radius 10 and turn 180 its very steep turnaround
		}
		return 0.0;
	}
}
