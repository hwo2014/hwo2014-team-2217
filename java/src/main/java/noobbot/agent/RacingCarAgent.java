package noobbot.agent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import noobbot.behavior.Behavior;
import noobbot.behavior.optimalPath.SwitchLeftBehavior;
import noobbot.behavior.optimalPath.SwitchRightBehavior;
import noobbot.behavior.throtle.ReleaseThrotleBehavior;
import noobbot.commands.Ping;
import noobbot.commands.SendMsg;
import noobbot.commands.Throttle;
import noobbot.physics.DistanceCalculator;
import noobbot.physics.Physics;
import noobbot.physics.TurboInfo;
import noobbot.track.Track;

public class RacingCarAgent {
	
	private enum BehaviorClass {THROTLE_USING_BEHAVIORS, SPECIAL_POWERS_BEHAVIORS, OPTIMAL_PATH_BEHAVIORS, SOCIAL_BEHAVIORS};

	private Map<BehaviorClass, LinkedList<Behavior>> robotBehaviorsHierarchy;
	
	private Track track;
	
	private CarPositionInTrack currentPosition;
	
	private Physics currentPhysics;
	
	private List<CarPositionInTrack> otherCarsInTrack;
	
	private TurboInfo turbo = null;
	
	private double currentThrotle = 1;
	
	private double previousThrotle = 0;

	private double previousThrotleDifference = 0;
	
	private double currentLap = 0;
	
	private int currentTick = 0;
	
	private String name;
	
	private boolean crashedFlag = false;

	public RacingCarAgent(Track track, String name) {
	    super();
	    
	    this.track = track;
	    this.name = name;
	    
	    currentPhysics = new Physics();
	    
	    this.robotBehaviorsHierarchy = new HashMap<>();
	    this.robotBehaviorsHierarchy.put(BehaviorClass.THROTLE_USING_BEHAVIORS, new LinkedList<Behavior>());
	    this.robotBehaviorsHierarchy.put(BehaviorClass.OPTIMAL_PATH_BEHAVIORS, new LinkedList<Behavior>());
	    
	    //THROTLE_USING_BEHAVIORS are the most important one, their first goal is not to crash (slow down, release throtle) and second goal is to maximize speed
	    
	    // realease gas pedal is most important behavior as it is activated only if possible crash, though it overrides all others
	    this.robotBehaviorsHierarchy.get(BehaviorClass.THROTLE_USING_BEHAVIORS).add(new ReleaseThrotleBehavior());
	    // if not maybe we need to just slow down
//	    this.robotBehaviorsHierarchy.get(BehaviorClass.THROTLE_USING_BEHAVIORS).add(new SlowDownBehavior());
//	    // if we are not too fast then maybe we need press pedal to the metal
//	    this.robotBehaviorsHierarchy.get(BehaviorClass.THROTLE_USING_BEHAVIORS).add(new GoMaxSpeedBehavior());
//	    // if we are not too fast then maybe we need pres pedal to the metal
//	    this.robotBehaviorsHierarchy.get(BehaviorClass.THROTLE_USING_BEHAVIORS).add(new SpeedUpBehavior());
//
	    this.robotBehaviorsHierarchy.get(BehaviorClass.OPTIMAL_PATH_BEHAVIORS).add(new SwitchRightBehavior());
	    this.robotBehaviorsHierarchy.get(BehaviorClass.OPTIMAL_PATH_BEHAVIORS).add(new SwitchLeftBehavior());
    }
	
	public void addTurbo(TurboInfo turbo) {
		this.turbo = turbo;
	}
	
	public void updatePhysics(double distance, double time,  double carAngle, double turnSteepness, double turnRadius) {
		double currentThrotleDifference = currentThrotle - previousThrotle;
		currentPhysics.updatePhysics(previousThrotleDifference, distance, time, carAngle, turnSteepness, turnRadius);
		previousThrotleDifference = currentThrotleDifference;
	}
	
	public void updateInputData(int gameTick, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, DistanceCalculator distanceCalculator) {
		this.currentTick = gameTick;
		this.currentPosition = position;
		this.otherCarsInTrack = otherCarsInTrack;
		if (currentLap != currentPosition.getLap()) {
			currentLap = currentPosition.getLap();
			System.out.println("Distance ran " + distanceCalculator.getTotalDistance());
		}
	}
	
	// kol kas tik pedalo valdymo behaviorai reik pagalvot kaip juos visus sudet i layerius ir apjungt nes galima tik viena zinute siusti
	// todel turbut svarbuma pvz ar rikiuotis ar didint greiti reikes daryt dinamiskai, galbut dar vienas behavioras, kuris ima visus activatintus ir sprendzia ar pan.
	public SendMsg nextAction() {
		LinkedList<Behavior> level0Behaviors = robotBehaviorsHierarchy.get(BehaviorClass.OPTIMAL_PATH_BEHAVIORS);
		LinkedList<Behavior> level1Behaviors = robotBehaviorsHierarchy.get(BehaviorClass.THROTLE_USING_BEHAVIORS);
		
		boolean tempCrashedFlag = crashedFlag;
		if (crashedFlag) {
			crashedFlag = false; // set to false, use crashed data only once
		}
		for (Behavior behavior : level0Behaviors) {
			if (behavior.isActivated(currentPhysics, currentPosition, otherCarsInTrack, track, tempCrashedFlag)) { // todo prideti ir Turbo informacija
				return behavior.act(currentThrotle, currentTick);
			}
		}
		for (Behavior behavior : level1Behaviors) {
			if (behavior.isActivated(currentPhysics, currentPosition, otherCarsInTrack, track, tempCrashedFlag)) { // todo prideti ir Turbo informacija
				SendMsg action = behavior.act(currentThrotle, currentTick);
				if (action instanceof Throttle) {
					previousThrotle = currentThrotle;
					currentThrotle = (double)((Throttle)action).msgData();
				}
				log();
				return action;
			} else {
				System.out.println("Press throtle to 1.0 >>> current tick " + currentTick);
				previousThrotle = currentThrotle;
				currentThrotle = 1.0;
				log();
				return new Throttle(1.0, currentTick);
			}
		}
		return new Ping();
	}
	
	public void crashed() {
		crashedFlag = true;
	}
	
	public double getCurrentThrotle() {
		return currentThrotle;
	}

	public double getPreviousThrotle() {
		return previousThrotle;
	}
	
	public double getCurrentVelocity() {
		return currentPhysics.getVelocity();
	}
	
	public double getPreviousVelocity() {
		return previousThrotle;
	}
	
	public String getName() {
		return name;
	}

	private void log() {
		System.out.println("CURRENT INFO >>> Turn " + currentPosition.getCurrentPiece().getTurnSteepness() + " pos turn " + currentPosition.getPositionTurnSteepness()  + 
				" Throtle " + currentThrotle + " Position " + currentPosition.getCurrentPieceId() + "+" + currentPosition.getPositionInPiece()
				+ " Lane " + currentPosition.getLaneId() + " Car Angle " + currentPosition.getAngle() + " Speed " + currentPhysics.getVelocity() +
				" Acceleration " + currentPhysics.getAcceleration() + " Acceleration power " + currentPhysics.getAccelerationPower() + 
				" Stopping power " + currentPhysics.getStoppingPower() + " Estimated friction " + currentPhysics.getFriction());
	}
}
