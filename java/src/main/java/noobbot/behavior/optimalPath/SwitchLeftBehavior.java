package noobbot.behavior.optimalPath;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.behavior.Behavior;
import noobbot.commands.SendMsg;
import noobbot.commands.SwitchLeft;
import noobbot.physics.Physics;
import noobbot.track.Track;
import noobbot.track.TrackPiece;

public class SwitchLeftBehavior implements Behavior {

	@Override
    public boolean isActivated(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, boolean crashed) {
	    // aktyvuoti kai galima pasukti, kai vaziuojama ne pacia desiniausia juosta ir kai nera trukdanciu auto
		TrackPiece currentPiece = position.getCurrentPiece();
		if (currentPiece.isHasSwitch() && position.getLaneId() < track.getMaxLaneId()) {
			if (currentPiece.getNext().getAngle() < 0 || currentPiece.getNext().getNext().getAngle() < 0 || currentPiece.getNext().getNext().getNext().getNext().getAngle() < 0) {
				return true;
			}
		}
		
		// desinej juostoj isskrenda is trasos ant to paprasto algo todel uzkomentavau
	    return false;
    }

	@Override
    public SendMsg act(double currentThrotle, int gameTick) {
	    return new SwitchLeft(gameTick);
    }

}
