package noobbot.behavior.turbo;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.behavior.Behavior;
import noobbot.behavior.DependentBehavior;
import noobbot.behavior.throtle.ReleaseThrotleBehavior;
import noobbot.behavior.throtle.SlowDownBehavior;
import noobbot.physics.Physics;
import noobbot.track.Track;

public abstract class AbstractTurboBehavior implements DependentBehavior {

	// turbo behavior allowed only if there were no slow down behavior activated
	@Override
    public boolean activate(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track,
            List<Behavior> activatedBehaviors) {
	    if (activatedBehaviors != null && activatedBehaviors.size() > 0) {
	    	boolean slowDownCommandIssued = activatedBehaviors.stream().anyMatch(
	    			behavior -> behavior instanceof SlowDownBehavior || behavior instanceof ReleaseThrotleBehavior);
	    	
	    	if (slowDownCommandIssued) {
	    		return false;
	    	}
	    }
    	return activateIfNeeded(physics, position, otherCarsInTrack, track);
    }

	public abstract boolean activateIfNeeded(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track);
}
