package noobbot.behavior;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.physics.Physics;
import noobbot.track.Track;

// behaviors which is dependent on other behaviors
public interface DependentBehavior extends Behavior {
	
    boolean activate(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, List<Behavior> activatedBehaviors);
	
	default boolean isActivated(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, boolean crashed) {
		return activate(physics, position, otherCarsInTrack, track, null);
	}

}
