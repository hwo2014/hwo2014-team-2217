package noobbot.behavior;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.commands.SendMsg;
import noobbot.learning.World;
import noobbot.physics.Physics;
import noobbot.track.Track;

public interface LearningCapableBehavior extends Behavior {

	default boolean isActivated(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, boolean crashed) {
		return returnLearnedAction(new World(physics, position, otherCarsInTrack, track, crashed));
	}
	
	/**
	 * Action space - true and false. That means this behavior learns to activate itself or not activate itself in provided state.
	 */
	boolean returnLearnedAction(World world);

	SendMsg act(double currentThrotle, int gameTick);

}
