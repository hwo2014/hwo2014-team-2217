package noobbot.behavior;

import java.util.List;

import noobbot.agent.CarPositionInTrack;
import noobbot.commands.SendMsg;
import noobbot.physics.Physics;
import noobbot.track.Track;

public interface Behavior {

	boolean isActivated(Physics physics, CarPositionInTrack position, List<CarPositionInTrack> otherCarsInTrack, Track track, boolean crashed);

	SendMsg act(double currentThrotle, int gameTick);

}
