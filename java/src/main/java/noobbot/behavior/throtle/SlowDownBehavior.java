package noobbot.behavior.throtle;

import noobbot.behavior.LearningCapableBehavior;
import noobbot.commands.SendMsg;
import noobbot.commands.Throttle;
import noobbot.learning.World;

public class SlowDownBehavior implements LearningCapableBehavior {

	@Override
	public boolean returnLearnedAction(World world) {
		
//		TrackPiece currentPiece = track.getTrackPieceById((int)position.getCurrentPiece());
//		
//		boolean nextPieceAngleMoreThan10 = currentPiece.getNext().getType() != Type.STRAIGHT &&
//				(-10 >= currentPiece.getNext().getAngle() || currentPiece.getNext().getAngle() >= 10);
//		boolean nextNextPieceAngleMoreThan35 = currentPiece.getNext().getNext().getType() != Type.STRAIGHT &&
//				(-35 >= currentPiece.getNext().getNext().getAngle() || currentPiece.getNext().getNext().getAngle() >= 35);
//		boolean nextNextNextPieceAngleMoreThan45 = currentPiece.getNext().getNext().getNext().getType() != Type.STRAIGHT &&
//				(-45 >= currentPiece.getNext().getNext().getNext().getAngle() || currentPiece.getNext().getNext().getNext().getAngle() >= 45);
//		
//		boolean tooFast = physics.getVelocity() > 17;
//		
//		if ((nextPieceAngleMoreThan10 || nextNextPieceAngleMoreThan35 || nextNextNextPieceAngleMoreThan45) && tooFast) {
//			return true;
//		}
	    return false;
    }

	@Override
    public SendMsg act(double currentThrotle, int gameTick) {
		System.out.println("Slow down >>> current tick " + gameTick);
		double newThrotle = currentThrotle - 0.17 > 0 ? currentThrotle - 0.17 : 0;
		return new Throttle(newThrotle, gameTick);
    }

}
