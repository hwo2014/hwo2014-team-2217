package noobbot.behavior.throtle;

import noobbot.behavior.LearningCapableBehavior;
import noobbot.commands.SendMsg;
import noobbot.commands.Throttle;
import noobbot.learning.PolicyTable;
import noobbot.learning.State;
import noobbot.learning.StateAction;
import noobbot.learning.World;

public class ReleaseThrotleBehavior implements LearningCapableBehavior {
	
	private PolicyTable stateActionPolicyTable = new PolicyTable();
	
	private StateAction previousStateAction = null;
	
	@Override
    public boolean returnLearnedAction(World world) {
		
		State currentState = world.getState();
		StateAction currentStateAction = new StateAction(currentState, stateActionPolicyTable.selectAction(currentState));
		// State is Car's position in track and track
		// next state (for car if true it should be closer by stopping power and if false should be further because of acceleration (work out approximate formulas))
		State estimatedNextState = world.getEstimatedNextState(currentState, currentStateAction.getAction());

		StateAction nextStateAction = new StateAction(estimatedNextState, stateActionPolicyTable.getBestAction(estimatedNextState));
		
		double thisQ = stateActionPolicyTable.getQValue(currentStateAction);
		double nextQ = stateActionPolicyTable.getQValue(nextStateAction);

		if (previousStateAction != null) {
			stateActionPolicyTable.updatePreviousQValue(previousStateAction, thisQ, nextQ, world.getRewardForPreviousAction(previousStateAction.getAction()));
		}

		// Set state to the new state and action to the new action.
		previousStateAction = currentStateAction;
		
		// after all calculations return selected action and in next iteration reward for it will be calculated
		
		return currentStateAction.getAction();
    }

	@Override
    public SendMsg act(double currentThrotle, int gameTick) {
		System.out.println("Release throtle >>> current tick " + gameTick);
	    return new Throttle(0, gameTick);
    }

}
