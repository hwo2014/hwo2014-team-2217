package noobbot.commands;

import java.util.HashMap;
import java.util.Map;

public class JoinRace extends SendMsg {
	
    public final String name;
    public final String key;
    public final Map<String, String> botId;
    public final String trackName;
    public final int carCount;

    public JoinRace(final String name, final String key, final String trackName) {
        this.name = name;
        this.key = key;
        this.botId = new HashMap<String, String>();
        botId.put("name", name);
        botId.put("key", key);
        this.trackName = trackName;
        this.carCount = 1;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }

	@Override
    protected Integer gameTick() {
	    return null;
    }

	public String getName() {
		return name;
	}

	public String getTrackName() {
		return trackName;
	}

	public int getCarCount() {
		return carCount;
	}
	
	
}