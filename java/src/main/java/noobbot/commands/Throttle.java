package noobbot.commands;

public class Throttle extends SendMsg {
	
    private final double value;
    
    private final Integer gameTick;

    public Throttle(double value, Integer gameTick) {
        this.value = value;
        this.gameTick = gameTick;
    }

    @Override
    public Object msgData() {
        return value;
    }

    @Override
    public String msgType() {
        return "throttle";
    }
    
    @Override
    public Integer gameTick() {
    	return gameTick;
    }
}
