package noobbot.commands;

public class SwitchRight extends SendMsg {
	
	private final Integer gameTick;

    public SwitchRight(Integer gameTick) {
        this.gameTick = gameTick;
    }
	
    @Override
    protected Object msgData() {
        return "Right";
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }

	@Override
    protected Integer gameTick() {
	    return gameTick;
    }
    
}