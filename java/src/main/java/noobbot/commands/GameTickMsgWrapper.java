package noobbot.commands;

public class GameTickMsgWrapper {
	
	public final String msgType;
	public final Object data;
	public final Integer gameTick;

	GameTickMsgWrapper(final String msgType, final Object data, final Integer gameTick) {
		this.msgType = msgType;
		this.data = data;
		this.gameTick = gameTick;
	}

	public GameTickMsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData(), sendMsg.gameTick());
	}
}