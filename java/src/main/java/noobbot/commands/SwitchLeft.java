package noobbot.commands;

public class SwitchLeft extends SendMsg {
	
	private final Integer gameTick;

    public SwitchLeft(Integer gameTick) {
        this.gameTick = gameTick;
    }
	
	@Override
	protected Object msgData() {
		return "Left";
	}

	@Override
	protected String msgType() {
		return "switchLane";
	}

	@Override
    protected Integer gameTick() {
	    return gameTick;
    }
	
}