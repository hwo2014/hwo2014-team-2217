package noobbot.commands;

import com.google.gson.Gson;

public abstract class SendMsg {
	
	public String toJson() {
		if (gameTick() != null) {
			return new Gson().toJson(new GameTickMsgWrapper(this));
		} else {
			return new Gson().toJson(new MsgWrapper(this));
		}
	}

	protected Object msgData() {
		return this;
	}
	
	protected abstract Integer gameTick();

	protected abstract String msgType();
}